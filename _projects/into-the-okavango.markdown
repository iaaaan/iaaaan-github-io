---
title:  "Into the Okavango"
subtitle: "Can we make open science accessible beyond academia? Into the Okavango turns a field expedition in Botswana into an immersive online experience and a user-friendly API."
description: "Partnering with ornithologist Dr. Steve Boyes, the Office for Creative Research developed a technology suite enabling the live collect, processing and visualization of thousands of data records from the field."

category: into-the-okavango
date:   "2017-11-02 14:54:00 -0500"
year: 2014-2017
with: "the Office for Creative Research"
with-link: "https://ocr.nyc/"
for: "National Geographic"
for-link: "http://www.nationalgeographic.com/"

role: "Design Lead and Frontend Dev Lead"
hasPosts: true
hasGallery: true

previous-project: twitter-aurora
next-project: c2-brain
press: <a target='_blank' href='https://www.theguardian.com/environment/radical-conservation/2015/may/28/expedition-source-okavango-delta'>The Guardian</a>, <a target='_blank' href='http://news.nationalgeographic.com/2015/05/150527-okavango-wilderness-project-delta-africa-wetland/'>National Geographic</a>, <a target='_blank' href='http://theterramarproject.org/thedailycatch/the-internet-of-earth-things-engineering-into-the-okavango/'>The Terramar Project</a>
---

{% include project-page.html %}