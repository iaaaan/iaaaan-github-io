---
title:  "Talks"
subtitle: "A sample of the talks I've given. Over the past years I've been invited to talk about information design, open data practice, and in one instance about the lack of diversity in tech."
category: talks
date:   "2017-04-13 14:54:00 -0500"
year: 2013-2017
hasPosts: true
hasGallery: false
type: "talks"

previous-project: c2-brain
next-project: elephant-atlas
---

{% include project-page.html %}