---
title:  "Twitter Aurora"
subtitle: "Aurora is a suite of data driven experiences visualizing the communities and conversations happening on the Twitter platform in real time."
description: "I joined the project in 2017 in order to consolidate its data processes, and develop new visualization tools that investigate the propagation of viral content across social media."
category: twitter-aurora
date:   "2020-01-04 09:54:00 -0500"
year: "2017-2019"
role: "Data Scientist & Information Designer"
hasPosts: true
hasGallery: true
with: "Twitter #Studio"

previous-project: specimen-box
next-project: into-the-okavango
---

{% include project-page.html %}