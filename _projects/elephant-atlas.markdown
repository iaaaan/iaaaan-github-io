---
title:  "Elephant Atlas"
subtitle: "Elephant populations are declining dramatically in most African countries. Can data visualization provide tools for addressing this issue, given the complex scientific and socio-economic background involved in wildlife conservation?"
description: "As part of the Great Elephant Census, the Office for Creative Research created an online visualization platform exploring the many factors behind the loss of elephant populations. With this project, it contributed to the [shutting down of the Chinese ivory market](http://news.nationalgeographic.com/2016/12/wildlife-watch-china-legal-ivory-market-african-elephants/) in 2017."

category: elephant-atlas
date:   "2016-10-02 19:13:40 +0200"
year: 2016
for: "Vulcan"
for-link: "http://www.vulcan.com/"
with: "The OCR"
with-link: "https://ocr.nyc/"

role: "Project and Design Lead"
hasPosts: true
hasGallery: true

previous-project: talks
next-project: radiant-city
press: <a target='_blank' href='http://www.cnn.com/2016/08/31/africa/great-elephant-census/'>CNN</a>, <a target='_blank' href='https://www.washingtonpost.com/news/worldviews/wp/2016/08/31/the-largest-ever-survey-of-elephants-in-africa-reveals-startling-declines/'>Washington Post</a>, <a target='_blank' href='https://www.theguardian.com/environment/2016/aug/31/poaching-drives-huge-30-decline-in-africas-savannah-elephants'>The Guardian</a>, <a target='_blank' href='http://news.nationalgeographic.com/2016/08/wildlife-african-elephants-population-decrease-great-elephant-census/'>National Geographic</a>
---

{% include project-page.html %}