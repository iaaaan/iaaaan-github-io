---
title:  "Specimen Box"
subtitle: "What do botnets tell us when given a voice? Specimen Box is a research tool that lets us explore the personality of those malware networks."
category: specimen-box
description: "Microsoft DCU tasked the Office for Creative Research with exploring the organic structures and behaviors uncovered by observing millions of zombie computers. The resulting application is an interactive, audiovisual installation living in Microsoft’s Cybercrime Center."

date:   "2014-02-01 00:00:00 -0500"
year: 2014
for: "Microsoft Cybercrime Unit"
for-link: "https://news.microsoft.com/presskits/dcu/#sm.00000b7w8fnigif79rsp9bqb4m5p1"
with: "The OCR"
with-link: "https://ocr.nyc/"

role: "Design Lead and Processing Dev Lead"
hasPosts: true
hasGallery: true

previous-project: floodwatch
next-project: twitter-aurora
press: <a target='_blank' href='https://www.wired.com/2014/12/sci-fi-worthy-interface-tracking-criminal-botnets/'>Wired</a>
---

{% include project-page.html %}